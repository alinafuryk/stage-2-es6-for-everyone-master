class Fighter {
    constructor(name, health, attack, defence) {
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.defence = defence;
    }

    get attack(){
        return this.attack;
    }

    get defence(){
        return this.defence;
    }
}